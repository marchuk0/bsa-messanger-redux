const API_URL = "https://edikdolynskyi.github.io/react_sources/messages.json";

export async function callApi() {
    return fetch(API_URL)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .catch((error) => {
            throw error;
        });

}