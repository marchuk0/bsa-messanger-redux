import * as React from "react";
import {Provider} from "react-redux";
import store from "./store";
import Messages from "./Messages";
import EditMessage from "./EditMessage";

class Chat extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Messages/>
                <EditMessage/>
            </Provider>
        );
    }
}

export default Chat;