import {SET_NEW_MESSAGE_ID, HIDE_PAGE, SHOW_PAGE} from './actionTypes';

const initialState = {
    messageId: null,
    isShown: false
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_NEW_MESSAGE_ID: {
            const id = action.payload.messageId;
            return {
                ...state,
                messageId: id
            }
        }
        case HIDE_PAGE: {
            return {
                ...state,
                isShown: false
            }
        }
        case SHOW_PAGE: {
            return {
                ...state,
                isShown: true
            }
        }
        default: {
            return state;
        }

    }
}