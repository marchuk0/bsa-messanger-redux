import React from "react";

import './EditMessage.css';
import {Button, Segment, TextArea} from "semantic-ui-react";
import {setNewMessageId, hideEdit} from "./actions";
import {updateMessage} from '../Messages/actions'
import {connect} from "react-redux";
import {focusEnd} from "../helperFunctions/textArea";

class EditMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ""
        }
        this.onChange = this.onChange.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onKey = this.onKey.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.messageId !== prevProps.messageId) {
            const message = this.props.messages.find((message) => message.id === this.props.messageId);
            this.setState({value: message ? message.text : ""});
        }

        if (this.props.isShown) {
            focusEnd("EditArea");
        } else {
            focusEnd("WriteArea");
        }
    }

    onChange(event) {
        this.setState({
            value: event.target.value
        });
    }

    onCancel() {
        this.props.setNewMessageId(null);
        this.props.hideEdit();
    }

    onSave() {
        if (this.state.value !== "") {
            this.props.updateMessage(this.props.messageId, this.state.value);
            this.props.setNewMessageId(null);
            this.props.hideEdit();
        }
    }

    onKey(event) {
        if (event.keyCode === 27) { // Escape
            this.onCancel();
            event.preventDefault();
        }
    }

    componentDidMount() {
        document.addEventListener("keydown", this.onKey, false);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.onKey, false);
    }


    render() {
        if (!this.props.isShown) {
            return null;
        }
        return (
            <div className={"EditContainer"}>
                <Segment className={"EditMessage"}>
                    <TextArea className="TextArea"
                              id="EditArea"
                              value={this.state.value}
                              onChange={this.onChange}/>
                    <Button.Group>
                        <Button basic onClick={this.onCancel}>Cancel</Button>
                        <Button onClick={this.onSave}>Save</Button>
                    </Button.Group>
                </Segment>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    isShown: state.messageForm.isShown,
    messageId: state.messageForm.messageId,
    messages: state.messages
});

const mapDispatchToProps = {
    setNewMessageId,
    hideEdit,
    updateMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);