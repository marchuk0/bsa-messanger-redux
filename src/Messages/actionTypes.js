export const FETCH_MESSAGES = "FETCH_MESSAGES";
export const CHANGE_LIKE = "CHANGE_LIKE";
export const DELETE_MESSAGE = "DELETE_MESSAGE";
export const UPDATE_MESSAGE = "UPDATE_MESSAGE";
export const CREATE_MESSAGE = "CREATE_MESSAGE";