import React from "react";
import {Button, Grid, Header, Icon, Image} from "semantic-ui-react";
import PropTypes from "prop-types";
import {deleteMessage, likeMessage} from '../../actions';
import {setNewMessageId, showEdit} from '../../../EditMessage/actions';
import {connect} from "react-redux";
import {getTimeString} from "../../../helperFunctions/dateHelper"
import './Message.css'


class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {flag: false}
        this.onDelete = this.onDelete.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onLike = this.onLike.bind(this);
    }

    onLike() {
        this.props.likeMessage(this.props.message.id);
    }

    onDelete() {
        this.props.deleteMessage(this.props.message.id);
    }

    onEdit() {
        this.props.setNewMessageId(this.props.message.id);
        this.props.showEdit();
    }

    onHover(flag) {
        this.setState({flag});
    }


    render() {
        const message = this.props.message;
        const userId = this.props.userId
        const newText = message.text.split('\n').map((item, i) => {
            return <p key={i}>{item}</p>;
        });

        let buttons;
        let header;
        let flexDirection;
        if (userId === message.userId) {
            buttons = (
                <Button.Group vertical>
                    <Button basic onClick={this.onDelete}>Delete</Button>
                    {this.state.flag ? <Button onClick={this.onEdit}>Edit</Button> : null}
                </Button.Group>);
            header = (
                <Header as='h5'>
                    {"Me at " + getTimeString(message.createdAt)}
                </Header>
            );
            flexDirection = "RightFlex";
        } else {
            buttons = (
                <Icon onClick={this.onLike}
                      name={message.isLiked ? 'heart' : 'heart outline'}
                      color='red'
                      size={"large"}/>
            );
            header = (
                <Header as='h5'>
                    <Image circular src={message.avatar}/>
                    {message.user + " at " + getTimeString(message.createdAt)}
                </Header>
            );
            flexDirection = "LeftFlex";
        }

        return (
            <div className={"FlexContainer " + flexDirection}>
                <div className="MessageBox">
                    <div onMouseEnter={() => this.onHover(true)}
                         onMouseLeave={() => this.onHover(false)}>
                        <Grid>
                            <Grid.Column width={7} floated={"left"}>
                                {header}
                                {newText}
                            </Grid.Column>
                            <Grid.Column width={4}>
                                {buttons}
                            </Grid.Column>
                        </Grid>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = {
    deleteMessage,
    setNewMessageId,
    showEdit,
    likeMessage
}

Message.propTypes = {
    userId: PropTypes.string.isRequired,
    message: PropTypes.object.isRequired
}

export default connect(null, mapDispatchToProps)(Message);