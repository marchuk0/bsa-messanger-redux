import React from "react";
import {Segment} from 'semantic-ui-react'
import PropTypes from 'prop-types';

import './Header.css';

class Header extends React.Component {

    render() {
        return (
            <div className={"HeaderContainer"}>
                <Segment.Group horizontal>
                    <Segment>{this.props.chatName}</Segment>
                    <Segment>{"Participants: " + this.props.usersCount}</Segment>
                    <Segment>{"MessageList: " + this.props.messagesCount}</Segment>
                    <Segment>{"Last message on: " + new Date(this.props.lastDate).toDateString()}</Segment>
                </Segment.Group>
            </div>
        )
    }
}

Header.propTypes = {
    chatName: PropTypes.string.isRequired,
    usersCount: PropTypes.number.isRequired,
    messagesCount: PropTypes.number.isRequired,
    lastDate: PropTypes.string.isRequired
}

export default Header;