import React from "react";
import {Divider, Segment} from "semantic-ui-react";
import Message from "../Message/Message";
import PropTypes from 'prop-types';
import * as actions from "../../actions"
import {setNewMessageId, showEdit} from "../../../EditMessage/actions";
import {connect} from "react-redux";
import './MessageList.css'

class MessageList extends React.Component {
    constructor(props) {
        super(props);
        this.onKey = this.onKey.bind(this);
    }


    onKey(event) {
        if (event.keyCode === 38) { // arrow up
            const messages = this.props.messages;
            messages.reverse();
            const message = messages.find((message) => message.userId === this.props.userId);
            if (message !== undefined) {
                this.props.setNewMessageId(message.id);
                this.props.showEdit();
            }
            messages.reverse();
            event.preventDefault();
        }
    }

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({behavior: "smooth"});
    }

    componentDidMount() {
        document.addEventListener("keydown", this.onKey, false);
        this.scrollToBottom();
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.onKey, false);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.messages.length < this.props.messages.length) { //scroll only when new message appears
            this.scrollToBottom();
        }
    }

    render() {
        const messages = this.props.messages;
        const userId = this.props.userId;
        const messagesList = [];
        for (let i = 0; i < messages.length; i++) {
            if (i === 0 || dateString(messages[i].createdAt) !== dateString(messages[i - 1].createdAt)) {
                messagesList.push(
                    <Divider key={messages[i].createdAt}
                             horizontal>{dateString(messages[i].createdAt)}
                    </Divider>)
            }
            messagesList.push(<Message key={messages[i].id}
                                       userId={userId}
                                       message={messages[i]}/>);

        }
        return (
            <div className="MessageListContainer">
                <Segment>
                    {messagesList}
                </Segment>

                <div style={{float: "left", clear: "both"}}
                     ref={(el) => {
                         this.messagesEnd = el;
                     }}>
                </div>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages
    }
};

const mapDispatchToProps = {
    ...actions,
    setNewMessageId,
    showEdit
};

function dateString(date) {
    return new Date(date).toDateString();
}

MessageList.propTypes = {
    userId: PropTypes.string.isRequired
}


export default connect(mapStateToProps, mapDispatchToProps)(MessageList);