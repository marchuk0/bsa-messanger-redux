import React from 'react'
import {Loader} from 'semantic-ui-react'

class Spinner extends React.Component {
    render() {
        return (
            <Loader active inline='centered'/>
        );
    }
}

export default Spinner
