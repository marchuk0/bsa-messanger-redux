import {FETCH_MESSAGES, CHANGE_LIKE, DELETE_MESSAGE, UPDATE_MESSAGE, CREATE_MESSAGE} from "./actionTypes";

export const fetchMessages = (messages) => ({
    type: FETCH_MESSAGES,
    payload: {
        messages
    }
});

export const likeMessage = (id) => ({
    type: CHANGE_LIKE,
    payload: {
        id
    }
});

export const deleteMessage = (id) => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const updateMessage = (id, newText) => ({
    type: UPDATE_MESSAGE,
    payload: {
        id,
        newText
    }
});

export const createMessage = (user, text) => ({
    type: CREATE_MESSAGE,
    payload: {
        user,
        text
    }
});
