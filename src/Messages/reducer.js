import {FETCH_MESSAGES, CHANGE_LIKE, DELETE_MESSAGE, UPDATE_MESSAGE, CREATE_MESSAGE} from "./actionTypes";
import {v4 as uuidv4} from 'uuid';

export default function (state = [], action) {
    switch (action.type) {
        case FETCH_MESSAGES: {
            return action.payload.messages;
        }
        case CHANGE_LIKE: {
            const id = action.payload.id;
            return state.map((message) => {
                if (message.id === id) {
                    return {
                        ...message,
                        isLiked: !message.isLiked
                    }
                } else {
                    return message;
                }
            });
        }
        case DELETE_MESSAGE: {
            const id = action.payload.id;
            return state.filter((message) => message.id !== id);
        }
        case UPDATE_MESSAGE: {
            const id = action.payload.id;
            const newText = action.payload.newText;
            return state.map((message) => {
                if (message.id === id) {
                    return {
                        ...message,
                        text: newText,
                        updatedAt: new Date().toJSON()
                    }
                } else {
                    return message;
                }
            });
        }
        case CREATE_MESSAGE: {
            const user = action.payload.user;
            const text = action.payload.text;
            const newMessage = {
                id: uuidv4(),
                ...user,
                text: text,
                createdAt: new Date().toJSON()
            };
            return [...state, newMessage];
        }

        default:
            return state;
    }
}