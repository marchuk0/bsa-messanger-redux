import React from 'react';
import Header from "./components/Header/Header";
import Spinner from "./components/Spinner/Spinner";
import SendMessageForm from "./components/SendMessageForm/SendMessageForm";
import MessageList from './components/MessageList/MessageList';

import {getMessages} from "../services/messagesService";
import {connect} from "react-redux";
import {fetchMessages} from './actions'
import './index.css';


class Messages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                userId: "5328dba1-1b8f-11e8-9629-c7eca82aa7bd",
                avatar: "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg",
                user: "Ben"
            },
            isDownloaded: false
        };

    }

    componentDidMount() {
        getMessages()
            .then(result => {
                result.sort(function (a, b) {
                    return new Date(a.createdAt) - new Date(b.createdAt)
                });
                this.props.fetchMessages(result);
                this.setState({isDownloaded: true});
            });
    }

    render() {
        if (!this.state.isDownloaded) {
            return (<Spinner/>);
        }
        const name = "MyChat";
        const usersCount = 12;
        const messagesCount = this.props.messages.length;
        const lastDate = messagesCount === 0 ? null : this.props.messages[messagesCount - 1].createdAt;

        return (
            <>
                <Header
                    chatName={name}
                    usersCount={usersCount}
                    messagesCount={messagesCount}
                    lastDate={lastDate}/>

                <MessageList userId={this.state.user.userId}/>

                <SendMessageForm user={this.state.user}/>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages
    }
};

const mapDispatchToProps = {
    fetchMessages
}

export default connect(mapStateToProps, mapDispatchToProps)(Messages);
