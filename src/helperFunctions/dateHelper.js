export const getTimeString = (date) => {
    date = new Date(date);
    const hours = date.getHours();
    const minutes = date.getMinutes();

    return "" + Math.floor(hours / 10) + hours % 10 +
        ":" + Math.floor(minutes / 10) + minutes % 10;
}