export const focusEnd = (id) => {
    const element = document.getElementById(id);
    if (element) {
        element.focus();
        element.setSelectionRange(element.value.length, element.value.length);
    }
}