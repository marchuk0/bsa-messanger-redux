import {combineReducers} from "redux";
import messages from '../Messages/reducer'
import messageForm from '../EditMessage/reducer';

const rootReducer = combineReducers({
    messages,
    messageForm
})

export default rootReducer;